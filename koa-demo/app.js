const Koa = require('koa') // 包装过的http
const KoaStaticCache = require('koa-static-cache') // 静态资源中间件
const Router = require('koa-router')
const co = require('co')
const Render = require('koa-swig')
const path = require('path')
const bodyParser = require('koa-bodyparser')

const app = new Koa() // 创建服务器

/* 处理静态资源：任何请求，首先通过 KoaStaticCache 中间件处理看是否是静态资源请求 */
app.use( KoaStaticCache(__dirname + '/static', {
  prefix: '/public' // 如果当前请求 url 是以 /public 开始的，则作为静态资源请求，映射到我服务器上的 /static 路径中的文件
}) )

/* 处理请求正文中的数据（处理post请求参数）*/
app.use(bodyParser())

/* 服务器重启会重置数据，所以可以写成一个本地文件，对文件进行增删改查（类似数据库的功能了） */
let datas = {
  maxId: 3,
  appName: 'TodoList',
  skin: 'index.css',
  tasks: [
    {id: 1, title: '测试任务1', done: true},
    {id: 2, title: '学习koa', done: false},
    {id: 3, title: '学习sql', done: false},
  ]
}

/* 设置模板引擎 */
app.context.render = co.wrap(Render({
  root: path.join(__dirname, 'views'),
  autoescape: true, // 数据是否编码（html）
  cache: false,
  // cache: 'memory', // 内存中缓存模板，下次访问就直接从内存中读取模板
  ext: 'html'
}))

/* router.routes() 也相当于一个中间件，请求 url 会经过我们定义的路由进行过滤 */
const router = new Router()

/* 首页 */
router.get('/', async ctx => {
  ctx.body =  await ctx.render('index.html', {datas})
})

/* 添加 */
router.get('/add', async ctx => {
  ctx.body =  await ctx.render('add.html', {datas})
})
router.post('/posttask', async ctx => {
  let cur_title = ctx.request.body.title || ''
  if(cur_title) {
    datas.tasks.unshift({
      id: ++datas.maxId,
      title: cur_title,
      done: false
    })
    ctx.body = await ctx.render('message', {
      msg: '添加成功',
      href: '/'
    })
  } else {
    ctx.body = await ctx.render('message', {
      msg: '请输入任务标题',
      href: 'javascript:history.back()'
    })
  }
})

/* 改变 */
router.get('/change/:id', ctx => {
  let cur_id = ctx.params.id
  datas.tasks.forEach(task => {
    if(task.id * 1 === cur_id * 1) {
      task.done = !task.done
    }
  })
  ctx.response.redirect('/')
})

/* 删除 */
router.get('/remove/:id', async ctx => {
  let cur_id = ctx.params.id
  datas.tasks = datas.tasks.filter(task => task.id * 1 !== cur_id * 1)
  ctx.body = await ctx.render('message', {
    msg: '删除成功',
    href: '/'
  })
})

app.use(router.routes())

app.listen(80, () => {
  console.log('启动成功...')
})
