const Koa = require('koa')
const KoaStaticCache = require('koa-static-cache')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const fs = require('fs')

const app = new Koa()

let datas = JSON.parse(fs.readFileSync('./data/data.json'))

/* 静态资源托管 */
app.use( KoaStaticCache(__dirname + '/static', {
  prefix: '/public',
  gzip: true
}) )

/* 处理body解析 */
app.use(bodyParser())

/* 路由 */
const router = new Router()
router.get('/', async ctx => {
  ctx.body = 'hello, this is a test!'
})
router.get('/todos', async ctx => {
  ctx.body = {
    code: 0,
    result: datas.todos
  }
})
router.post('/add', async ctx => {
  let title = ctx.request.body.title || ''
  if(!title) {
    ctx.body = {
      code: 1,
      result: '请传入任务标题'
    }
  } else {
    let newTask = {
      id: ++datas._id,
      title,
      done: false
    }
    datas.todos.unshift(newTask)
    ctx.body = {
      code: 0,
      result: newTask
    }
    fs.writeFileSync('./data/data.json', JSON.stringify(datas))
  }
})
router.post('/toggle', async ctx => {
  let id = ctx.request.body.id * 1 || 0
  if(!id) {
    ctx.body = {
      code: 1,
      result: '请传入id'
    }
  } else {
    let todo = datas.todos.find(todo => todo.id * 1 === id)
    todo.done = !todo.done
    ctx.body = {
      code: 0,
      result: '修改成功'
    }
    fs.writeFileSync('./data/data.json', JSON.stringify(datas))
  }
})
router.post('/remove', async ctx => {
  let id = ctx.request.body.id * 1 || 0
  if(!id) {
    ctx.body = {
      code: 1,
      result: '请传入id'
    }
  } else {
    datas.todos = datas.todos.filter(todo => todo.id * 1 !== id)
    ctx.body = {
      code: 0,
      result: '删除成功'
    }
    fs.writeFileSync('./data/data.json', JSON.stringify(datas))
  }
})

app.use(router.routes())

app.listen(80, () => {
  console.log('启动成功...')
})
